package com.example.pd_2;

import androidx.appcompat.app.AppCompatActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Button;
import android.widget.Toast;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;

public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        final boolean[] PreviousAnswers = new boolean[] { false, false, false };
        final boolean[] CurrentAnswers = new boolean[] { false, false, false };

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 2. Aktivitates Poga:
        Button button_activity_2 = findViewById(R.id.button_activity_2);
        button_activity_2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent1 = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(intent1);
            }
        });

        // Dialoga Poga:
        Button button_dialog = findViewById(R.id.button_dialog);
        button_dialog.setOnClickListener(new View.OnClickListener()
        {


            @Override
            public void onClick(View view)
            {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setTitle(R.string.dialog_title);

                // Dialoga Saturs (Saraksts):
                final String [] DialogItems = getResources().getStringArray(R.array.dialog_group_members);

                builder1.setMultiChoiceItems(R.array.dialog_group_members, CurrentAnswers, new DialogInterface.OnMultiChoiceClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked)
                    {
                        Toast.makeText(MainActivity.this, DialogItems[which] + " " + (isChecked ? getString(R.string.dialog_item_selected) : getString(R.string.dialog_item_deselected)), Toast.LENGTH_LONG).show();
                        CurrentAnswers[which] = isChecked ? true : false;
                    }
                });

                // Dialoga Poga - Labi
                builder1.setPositiveButton(R.string.dialog_button_accept, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        Toast.makeText(MainActivity.this, getString(R.string.dialog_button_clicked_accept), Toast.LENGTH_SHORT).show();

                        for (int i = 0; i < 3; i++)
                            PreviousAnswers[i] = CurrentAnswers[i];
                    }
                });

                // Dialoga Poga - Atcelt
                builder1.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        Toast.makeText(MainActivity.this, getString(R.string.dialog_button_clicked_cancel), Toast.LENGTH_SHORT).show();

                        for (int i = 0; i < 3; i++)
                            CurrentAnswers[i] = PreviousAnswers[i];
                    }
                });

                // Izveidojam Dialogu:
                AlertDialog dialog = builder1.create();
                dialog.show();
            }
        });
    }
}