package com.example.pd_2;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;

public class Main2Activity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        // 1. Aktivitates Poga:
        Button button_activity_1 = findViewById(R.id.button_activity_1);
        button_activity_1.setText(getResources().getString(R.string.button_activity_1));

        button_activity_1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });
    }
}